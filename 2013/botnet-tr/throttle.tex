\section{Throttling} \label{sec:throttle}

Since the primary concern from the point of view of the other users of
Tor is the {\em rate} at which botnet nodes consume the collective
computing resources of the relays, another set of potential solutions
is to attempt to throttle or otherwise limit the rate of requests from
the botnet.  Two key points to recall in evaluating solutions from
this class are that (i) in many ways the botnet has more resources
available than the set of all regular Tor clients and (ii) neither
bots nor the C\&C server are constrained to follow the standard Tor
algorithms, although the current implementations may do so.

\subsection{Can we throttle by cost?} \label{sec:cost}

One way to control the rate at which circuit building requests enter
the network is by making it costly to send them.  Tor could do this by
requiring proof of the expenditure of a scarce resource, for example,
human attention, processor time, bitcoins, and so on.  If the cost to
build a circuit or connect to a hidden service can be correctly
allocated it could be the case that ordinary users and services can
easily afford the cost while the price for a botnet becomes
prohibitive.  Depending on the resource used, correctly
allocating the cost is an important research question; we consider the
problem for Proof of Work (CPU-based) and CAPTCHA (human
attention-based) systems below.

Besides the cost allocation problem, another technical challenge is
ensuring that resources can't be double-spent, so that each resource
expenditure in a given time period only authorizes a single circuit or
hidden service connection.    Several approaches exist, but each would
require further investigation:
\begin{itemize}
\item Make the unit of pay cover a single circuit extension and have
  one of the relays extending the circuit issue a challenge back to
  the client, which then must be answered before the {\sc create} (or
  {\sc extend}) cell is processed, similar to the scheme described by
  Barbera {\em et al.}~\cite{esorics13-cellflood}.  This has the
  unfortunate side effect of adding an extra round-trip time to every
  circuit-building request.  Finding a way to hide this extra
  round-trip time could make it a viable alternative, for some
  resources. 

\item Relay descriptors could include ``puzzle specifications'' that
  describe what the challenge will be for a given time period,
  requiring a method to prevent ``precomputing'' a batch of payments
  before the time period; how to solve this problem is an open
  question.

\item Another method would use an extra trusted server that verifies
  resource expenditures and issues relay- and time period-specific
  signed tokens, similar to {\em ripcoins}~\cite{reiter2005building}
  or the tokens in {\em BRAIDS}~\cite{ccs10-braids}.  Using blinded
  tokens would limit the trust required in the server so that it can't
  compromise anonymity, and relay-specificity would
  allow each relay to verify that tokens aren't double-spent. However,
  this adds an extra signature-verification to the task of onion-skin processing
  and another server and key that must be maintained.
\end{itemize}
All of these solutions also require adding extra verification to the
onion-skin handshake, incurring the additional risk implied in changes
to the core Tor protocol.

\subsubsection{Proof of work (proves once more not to work?)}

When the resource in question is processor time and challenges are,
e.g. hashcash~\cite{back2002hashcash} targets, the cost allocation strategy should
dictate that the hidden service must pay a cost for each connection,
since bots clients and normal hidden service clients will have
essentially identical use profiles (from the point of view of relays)
and computational resources.   On the other hand, the C\&C hidden server(s)
will collectively initiate many more circuits than any single
``normal'' hidden server.

The key security challenge when considering an adaptive botmaster's
response to this approach is the ``chain-proving'' attack (by analogy
to chain voting~\cite{jones2005chain}).  In this attack, the C\&C
server solves the first challenge it receives when a bot contacts the
hidden service, but then on each additional challenge, the {\em
  previous bot} is asked to solve the puzzle in time to allow the {\em
  next bot} to connect.   In principle the difference in latencies
(caused by the need to pass a puzzle to the bot through Tor) could
potentially be detected, but an adaptive botmaster could well build
shorter circuits, and employ multiple bots in an effort to reduce the
time needed to solve a ``proof of work'' puzzle.  

\subsubsection{CAPTCHAs}

If CAPTCHAs are used to verify expenditure of human attention, the
relative cost allocation should change to favor the client: clients of
most hidden services will have human users, while hidden servers will
not.  This raises additional technical problems, such as how CAPTCHAs can be
served through Tor without a GUI interface, how a user's solution can be transferred to the
hidden service without violating privacy or allowing overspending, and
how to deal with the needs of completely headless services where
neither the HS client nor the HS server have a user's attention to
give.  

Another technical challenge to deploying CAPTCHAs is how to defeat
computationally expensive automated solvers.  Most
commercially-deployed web CAPTCHAs can be solved with success rates on
the order of 1-10\% per challenge, and the typical service mitigates
this by temporarily blacklisting an IP address after a small number of
attempts.  With anonymous users, this becomes a more challenging
problem to solve; without blacklisting a bot can simply attempt as
many CAPTCHAs as necessary to obtain an automated solution.

\subsubsection{Network Contributions}

As in several Tor incentive schemes~\cite{incentives-fc10,ccs10-braids,acsac11-tortoise,ndss13-lira}
we could require proof of contributions to the network to receive
high performance at volume, while providing a level of performance to
non-relay users that would be acceptable for normal clients but would
significantly impede the activity of a hidden C\&C server.  However,
this would have significant impact on the security of regular Tor
users since it might simply incentivize a botmaster to run many
compromised relays.

\subsection{Can we throttle at the entry guard?} \label{sec:guard}

A more direct approach would be to simply have guard nodes rate-limit
the number of {\sc extend} cells they will process on a given
connection.   If the entry guard won't process the {\sc extend} cell
needed to build a circuit, the hidden server's OP can't flood the
network with onion-skins.  Notice that this measure won't prevent
{\em bots} from flooding the network with circuit requests; it simply
makes the network ineffective from the botmaster's standpoint and
thus, hopefullly, encourages botmasters to find some other C\&C
channel that causes less stress on the Tor network.

Effective circuit throttling at the guard node faces a number of
challenges, however.  Biryukov {\em et al}~\cite{hscontent2013} found
that the most popular hidden services see over 1000 requests per hour;
if we assume that these hidden services won't modify Tor's default
behavior, then guard nodes need to allow each client to extend over
300 circuits per hour; but since there are currently over 1200 relays
with the guard flag, a single C\&C server run by an adaptive botmaster
could build 360 000 circuits per hour at this rate.  We could decrease
the cap and try to make it easier for busy hidden servers to increase
their guard count, but this significantly increases the chance that a
hidden server chooses a compromised guard and can be deanonymized.

One possibilty would be to use {\em assigned guards.}  In this
approach, ordinary clients would pick guards as usual, and guards
would enforce a low rate-limit $r_{\text{default}}$ on circuit
extensions, for example 30 circuits per hour.\footnote{Naturally, finding the
  right number to use for this default rate is also an interesting
  research challenge: a very low rate-limit could prevent bots from
  flooding the network but might also disrupt legitimate hidden
  service clients}
 OPs that need to build circuits at a higher rate
$r_{\text{server}}$ -- say, 2000 per hour -- could follow a
cryptographic protocol that would result in a verifiable token that
assigns a deterministic, but unpredictable, guard node for the OP when
running on a given IP address.  These OPs could then show this token
to the assigned guard and receive a level of service sufficient for a
busy hidden server, but not for a flood of circuit extensions.  An
example of this type of protocol appears as Protocol 3 (section 3.3)
in the {\em BRAIDS} design by Jansen {\em et
  al.}~\cite{ccs10-braids}.  The rates $r_{\text{default}}$ and
$r_{\text{server}}$ could appear in the network consensus, to allow
adjustments for the volume of traffic in the network.
Figure~\ref{fig:throttle} shows the result of simulating this strategy
with $r_{\text{default}} = 10$ and $r_{\text{server}}=2000$ using the
{\em shadow} simulator~\cite{shadow-ndss12}; despite nearly identical
bandwidth usage, the throttled simulation has performance
characteristics similar to the simulation with no botnet.

\begin{figure}[t]
\vspace{-20pt}
\begin{tabular}{ccc}
\includegraphics[width=0.33\textwidth]{results-ttlb-bulk.pdf}&
\includegraphics[width=0.33\textwidth]{results-circs-buildtime-cdf.pdf}&
\includegraphics[width=0.33\textwidth]{results-tput-read.pdf}\\
(a) & (b) & (c)
\end{tabular}
\vspace{-10pt}
\caption{Results of guard throttling: 20 relays, 200 clients, 500
  bots. (a) 5MiB download times, (b) Circuit build times, (c) Total
  bytes read}\label{fig:throttle}
\vspace{-20pt}
\end{figure}

An additional technical challenge associated with guard throttling is
the need to enforce the use of entry guards when building circuits.
If the C\&C server joins the network as a relay, {\sc create} cells
coming from the hidden service would be indistinguishable from {\sc
  create} cells coming from other circuits running through the relay,
effectively circumventing the rate limit.  In principle this could be
detected by a distributed monitoring protocol, but designing secure
protocols of this type that avoid adversarial manipulation has proven
to be a difficult challenge.

\subsection{Can we throttle by Tor version?}

Another strategy that may be useful against a nonadaptive botmaster is
to throttle (perhaps temporarily) by Tor version.  In this strategy,
nodes running old versions of Tor could be either severely throttled
or completely ignored by nodes with the latest software.  Since relays
are typically kept up-to-date and most clients access Tor using the
Tor Browser Bundle (which can inform clients on start-up when new
versions are recommended or, in the future, might incorporate
auto-update functionality) the impact on typical users would be
minimal, but a bot that packages the Tor software may not be able to
respond to updates.  The technical challenges here involve designing 
reliable methods to generate new software versions,
(sufficently) secure protocols to check that a client is running the
software, and analyzing the use cases that might affect clients'
ability to update frequently and the impact of throttling in these cases.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "botnet-tr"
%%% End: 
