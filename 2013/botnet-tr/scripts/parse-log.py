#!/usr/bin/python

log_file_list_file = "log_files.txt"
import scipy.stats

def parse_time(s):
    (day, hr) = s.split('.')
    return 24*(int(day)-1) + int(hr)

def process_log_file(lf):
    time, version = 0, ''
    o_skins, fails = 0, 0
    with open(lf, "r") as log:
        for line in log:
            field = line.strip().split('\t')
            if field[0] == 'TIME': time = parse_time(field[1])
            if field[0] == 'VERSION': version = field[1]
            if field[0] == 'CIRCUIT' and field[3] == 'GENERAL':
                if field[4] == 'BUILT': o_skins += 3
                if field[4] == 'FAILED':
                    partial_circ = field[2].split(',')
                    o_skins += len(partial_circ)+1
                    fails += 1
	if version == '':
	    time = parse_time(lf[4:])
	    print '0.x (git-'')', time, '-', '-', '-', '-', '-'
	else:
            p = float(fails)/o_skins
            (lower, upper) = scipy.stats.beta.interval(0.95, fails, o_skins-fails+1) # Clopper-Pearson "Exact" binomial confidence interval
            print version, time, o_skins, fails, p, lower, upper
    
                
with open(log_file_list_file, "r") as f:
    for log_file in f:
        lf_name = log_file.strip()
        process_log_file(lf_name)
